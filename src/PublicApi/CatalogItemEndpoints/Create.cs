﻿using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Microsoft.SampleAPI.ApplicationCore.Entities;
using Microsoft.SampleAPI.ApplicationCore.Exceptions;
using Microsoft.SampleAPI.ApplicationCore.Interfaces;
using Microsoft.SampleAPI.ApplicationCore.Specifications;
using Swashbuckle.AspNetCore.Annotations;
using System.Threading;
using System.Threading.Tasks;

namespace Microsoft.SampleAPI.PublicApi.CatalogItemEndpoints
{
    public class Create : BaseAsyncEndpoint
        .WithRequest<CreateCatalogItemRequest>
        .WithResponse<CreateCatalogItemResponse>
    {
        private readonly IRepository<CatalogItem> _itemRepository;
        private readonly IUriComposer _uriComposer;

        public Create(IRepository<CatalogItem> itemRepository,
            IUriComposer uriComposer)
        {
            _itemRepository = itemRepository;
            _uriComposer = uriComposer;
        }

        [HttpPost("api/catalog-items")]
        [SwaggerOperation(
            Summary = "Creates a new Catalog Item",
            Description = "Creates a new Catalog Item",
            OperationId = "catalog-items.create",
            Tags = new[] { "CatalogItemEndpoints" })
        ]
        public override async Task<ActionResult<CreateCatalogItemResponse>> HandleAsync(CreateCatalogItemRequest request, CancellationToken cancellationToken)
        {
            var response = new CreateCatalogItemResponse(request.CorrelationId());

            var catalogItemNameSpecification = new CatalogItemNameSpecification(request.Name);
            var existingCataloogItem = await _itemRepository.CountAsync(catalogItemNameSpecification, cancellationToken);
            if (existingCataloogItem > 0)
            {
                throw new DuplicateException($"A catalogItem with name {request.Name} already exists");
            }

            var newItem = new CatalogItem(request.CatalogTypeId, request.CatalogBrandId, request.Description, request.Name, request.Price, request.PictureUri);
            newItem = await _itemRepository.AddAsync(newItem, cancellationToken);

            if (newItem.Id != 0)
            {
                newItem.UpdatePictureUri("eCatalog-item-default.png");
                await _itemRepository.UpdateAsync(newItem, cancellationToken);
            }

            var dto = new CatalogItemDto
            {
                Id = newItem.Id,
                CatalogBrandId = newItem.CatalogBrandId,
                CatalogTypeId = newItem.CatalogTypeId,
                Description = newItem.Description,
                Name = newItem.Name,
                PictureUri = _uriComposer.ComposePicUri(newItem.PictureUri),
                Price = newItem.Price
            };
            response.CatalogItem = dto;
            return response;
        }


    }
}
