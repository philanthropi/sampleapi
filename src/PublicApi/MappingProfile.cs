﻿using AutoMapper;
using Microsoft.SampleAPI.ApplicationCore.Entities;
using Microsoft.SampleAPI.PublicApi.CatalogBrandEndpoints;
using Microsoft.SampleAPI.PublicApi.CatalogItemEndpoints;
using Microsoft.SampleAPI.PublicApi.CatalogTypeEndpoints;

namespace Microsoft.SampleAPI.PublicApi
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CatalogItem, CatalogItemDto>();
            CreateMap<CatalogType, CatalogTypeDto>()
                .ForMember(dto => dto.Name, options => options.MapFrom(src => src.Type));
            CreateMap<CatalogBrand, CatalogBrandDto>()
                .ForMember(dto => dto.Name, options => options.MapFrom(src => src.Brand));
        }
    }
}
