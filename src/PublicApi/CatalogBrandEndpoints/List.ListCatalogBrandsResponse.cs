﻿using System;
using System.Collections.Generic;

namespace Microsoft.SampleAPI.PublicApi.CatalogBrandEndpoints
{
    public class ListCatalogBrandsResponse : BaseResponse
    {
        public ListCatalogBrandsResponse(Guid correlationId) : base(correlationId)
        {
        }

        public ListCatalogBrandsResponse()
        {
        }

        public List<CatalogBrandDto> CatalogBrands { get; set; } = new List<CatalogBrandDto>();
    }
}
