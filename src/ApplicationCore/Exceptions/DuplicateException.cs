﻿using System;

namespace Microsoft.SampleAPI.ApplicationCore.Exceptions
{

    public class DuplicateException : Exception
    {
        public DuplicateException(string message) : base(message)
        {
            
        }
       
    }
}
