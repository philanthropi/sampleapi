﻿using Microsoft.SampleAPI.ApplicationCore.Interfaces;

namespace Microsoft.SampleAPI.ApplicationCore.Entities
{
    public class CatalogBrand : BaseEntity, IAggregateRoot
    {
        public string Brand { get; private set; }
        public CatalogBrand(string brand)
        {
            Brand = brand;
        }
    }
}
