﻿using Microsoft.SampleAPI.ApplicationCore.Entities.OrderAggregate;
using System.Threading.Tasks;

namespace Microsoft.SampleAPI.ApplicationCore.Interfaces
{
    public interface IOrderService
    {
        Task CreateOrderAsync(int basketId, Address shippingAddress);
    }
}
