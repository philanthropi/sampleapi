﻿using System.Threading.Tasks;

namespace Microsoft.SampleAPI.ApplicationCore.Interfaces
{
    public interface ITokenClaimsService
    {
        Task<string> GetTokenAsync(string userName);
    }
}
