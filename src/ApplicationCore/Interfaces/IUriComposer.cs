﻿namespace Microsoft.SampleAPI.ApplicationCore.Interfaces
{
    public interface IUriComposer
    {
        string ComposePicUri(string uriTemplate);
    }
}
