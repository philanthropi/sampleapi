﻿using Ardalis.Specification;
using Microsoft.SampleAPI.ApplicationCore.Entities;

namespace Microsoft.SampleAPI.ApplicationCore.Specifications
{
    public class CatalogItemNameSpecification : Specification<CatalogItem>
    {
        public CatalogItemNameSpecification(string catalogItemName)
        {
            Query.Where(item => string.Compare(catalogItemName, item.Name, true) == 0);
        }
    }
}
