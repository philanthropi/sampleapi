﻿using Ardalis.Specification;
using Microsoft.SampleAPI.ApplicationCore.Entities.OrderAggregate;

namespace Microsoft.SampleAPI.ApplicationCore.Specifications
{
    public class CustomerOrdersWithItemsSpecification : Specification<Order>
    {
        public CustomerOrdersWithItemsSpecification(string buyerId)
        {
            Query.Where(o => o.BuyerId == buyerId)
                .Include(o => o.OrderItems)
                    .ThenInclude(i => i.ItemOrdered);
        }
    }
}
