﻿using Ardalis.Specification;
using Microsoft.SampleAPI.ApplicationCore.Entities.OrderAggregate;
using Microsoft.SampleAPI.ApplicationCore.Interfaces;
using Microsoft.SampleAPI.ApplicationCore.Specifications;
using Moq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Microsoft.SampleAPI.UnitTests.MediatorHandlers.OrdersTests
{
    public class GetOrderDetails
    {
        private readonly Mock<IReadRepository<Order>> _mockOrderRepository;

        public GetOrderDetails()
        {
            var item = new OrderItem(new CatalogItemOrdered(1, "ProductName", "URI"), 10.00m, 10);
            var address = new Address(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>());
            Order order = new Order("buyerId", address, new List<OrderItem> { item });

            _mockOrderRepository = new Mock<IReadRepository<Order>>();
            _mockOrderRepository.Setup(x => x.GetBySpecAsync(It.IsAny<OrderWithItemsByIdSpec>(),default))
                .ReturnsAsync(order);
        }
    }
}
