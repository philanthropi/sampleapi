﻿using Ardalis.Specification;
using Microsoft.SampleAPI.ApplicationCore.Entities.OrderAggregate;
using Microsoft.SampleAPI.ApplicationCore.Interfaces;
using Moq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Microsoft.SampleAPI.UnitTests.MediatorHandlers.OrdersTests
{
    public class GetMyOrders
    {
        private readonly Mock<IReadRepository<Order>> _mockOrderRepository;

        public GetMyOrders()
        {
            var item = new OrderItem(new CatalogItemOrdered(1, "ProductName", "URI"), 10.00m, 10);
            var address = new Address(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>());
            Order order = new Order("buyerId", address, new List<OrderItem> { item });

            _mockOrderRepository = new Mock<IReadRepository<Order>>();
            _mockOrderRepository.Setup(x => x.ListAsync(It.IsAny<ISpecification<Order>>(),default)).ReturnsAsync(new List<Order> { order });
        }
    }
}
