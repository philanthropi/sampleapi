Hi and welcome to our sample API. Before you get started, here are few tips to start.

# Getting Started

This solution is intended to be used with VS Code https://code.visualstudio.com/download and requires dotnet 5.0 https://dotnet.microsoft.com/download/dotnet/5.0
We have included 'launch.json' and 'tasks.json' to make things easier. Opening this folder in VS code will let you launch the app, and have quick access to the build and test tasks without any additional configuration steps. Launching the app should display Swagger with a few basic endpoints.
This solution uses an in-memory database which is seeded with data on Startup using CatalogContextSeed. There is existing seed data you can use to check things out in Swagger.

# The Actual Assignment

1. 
- We would like you to write a new endpoint in the PublicApi project, lets call it 'GetCatalogItemsByCatalogType'. We want this endpoint to do exactly what it sounds like, return a list of CatalogItems from a requested CatalogTypeId.
- We need tests to cover this endpoint. Please add Functional tests. There are existing tests you can reference and these tests use the seed data.


2. 
- Write a 'Delete' endpoint to delete a specific CatalogItem
- Add Functional tests to cover this new endpoint


We understand that this specific architecture may be new to you, and that is OK. This exercise is intended to just see what you can come up with and is not meant to take a huge chunk of your time.

# Testing

All of the existing tests should be passing. You can use the Test tasks to run all Functional, Integration, and Unit tests.

# VSCode Tasks

**Build** - Compiles all projects
**Test** - Run all tests in-memory